import Link from 'next/link'

export default function Backhome() {
	return (
		<Link href="/">
			<a><b>&larr; Back home</b></a>
		</Link>
	)
}