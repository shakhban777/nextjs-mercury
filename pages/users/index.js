import Link from 'next/link'
import styles from '../../styles/Home.module.css'
import Backhome from '../../components/home'

function Users({ data }) {
	return (
		<main className={styles.container}>
			<h1 className={styles.title}>User List</h1>
			<ul>
				{data.map((user) => (
					<li key={user.id}>
						<Link href={`/users/${user.id}`}>
							<a>{user.name}</a>
						</Link>
					</li>
				))}
			</ul>
			<Backhome />
		</main>
	)
}

export async function getStaticProps() {
	const response = await fetch(`https://jsonplaceholder.typicode.com/users`)
	const data = await response.json()

	return {
		props: { data }
	}
}

export default Users