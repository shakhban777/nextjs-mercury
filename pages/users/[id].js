import { useRouter } from 'next/router'
import styles from '../../styles/Home.module.css'
import Link from 'next/link'

export default function User({ user }) {
	const { query } = useRouter()
	return (
		<main className={styles.container}>
			<div className={styles.card}>
				<h1>User with id {query.id}</h1>
				<p>Name: {user.name}</p>
				<p>Email: {user.email}</p>
				<p>Phone: {user.phone}</p>
				<p>Website: {user.website}</p>
			</div>
			<Link href="/users">
				<a>&larr; Back to User List</a>
			</Link>
		</main>
	)
}

export async function getServerSideProps({ params }) {
	const response = await fetch(`https://jsonplaceholder.typicode.com/users/${params.id}`)
	const user = await response.json()
	return {
		props: { user }
	}
}