import { signIn, signOut, useSession } from "next-auth/client"
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import Image from 'next/image'

export default function Home() {
  const [session, loading] = useSession()

  return (
    <div className={styles.container}>

      <Head>
        <title>NextJS Mercury</title>
        <meta name="description" content="Homework Magomedov Shakhban" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.grid}>
        <h1 className={styles.title}>NextJS</h1>

        {!session && (
          <div className={styles.profile}>
            {loading && <p>Loading..</p>}
            <h3>You are Not signed in </h3>
            <button onClick={() => signIn("google", { callbackUrl: "https://nextjs-shakhban.vercel.app/" })}>Sign in</button>
          </div>
        )}

        {session && (
          <>
            <div className={styles.profile}>
              <Image src={session.user.image}
                className={styles.avatar}
                width={60}
                height={60}
                alt='avatar' />
              <span>{session.user.name}</span>
              <button onClick={() => signOut()}>Sign out</button>
            </div>
          </>
        )}
      </div>

      <main className={styles.main}>
        <ul>
          <li>
            <Link href='/public'>
              <a>Weather in San Francisco</a>
            </Link>
          </li>
          <li>
            <Link href='/private'>
              <a>My Profile</a>
            </Link>
          </li>
          <li>
            <Link href='/users'>
              <a>SSR Demo</a>
            </Link>
          </li>
        </ul>
        <Link href="/docs">
          <a className={styles.card}>
            <h2>API Routes &rarr;</h2>
            <p>
              Documentation of NextJS
            </p>
          </a>
        </Link>
      </main>
    </div>
  )
}