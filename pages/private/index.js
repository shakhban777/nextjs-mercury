import { signIn, signOut, useSession } from "next-auth/client"
import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Backhome from '../../components/home'
import Image from 'next/image'

export default function Private() {
  const [session, loading] = useSession()

  return (
    <div className={styles.container}>

      <main className={styles.main}>
        {!session && (
          <>
            <Head>
              <title>Access Denide!</title>
            </Head>
            <div className={styles.main}>
              <h3>Access Denide! You are not Sined in!</h3>
              <button
                onClick={() => signIn("google", { callbackUrl: "https://nextjs-shakhban.vercel.app/private" })}>
                Sign in
              </button>
            </div>
          </>
        )}

        {session && (
          <div className={styles.main}>
            <Head>
              <title>Private Page</title>
            </Head>
            <h2>Welcome back, {session.user.name}</h2>
            <div className={styles.card}>
              {loading && <p>Loading..</p>}
              <h3>Your privte information</h3>
				  <Image src={session.user.image}
				  			className={styles.avatar}
				  			width={60}
							height={60}
							alt='avatar'/>
              <h4>Signed in as {session.user.name}</h4>
              <h4>Email: {session.user.email}</h4>
            </div>
            <button onClick={() => signOut()}>Sign out</button>
          </div>
        )}
        <Backhome />
      </main>
    </div>
  )
}