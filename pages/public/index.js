import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Link from 'next/link'
import Backhome from '../../components/home'

export default function Public() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Weather in San Francisco</title>
      </Head>
      <iframe src="https://weather-app-twa.web.app/geo?coords=geo%3A37.786971%2C-122.399677"
        frameBorder="0"
        width='350px'
        height='85%'
        scrolling='no'
        seamless >
      </iframe>
      <Backhome />
    </div>
  )
}