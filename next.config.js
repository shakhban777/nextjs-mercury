module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return {
      fallback: [
        {
          source: '/:path*',
          destination: `https://nextjs.org/:path*`
        }
      ]
    };
  },
  images: {
	domains: ['lh3.googleusercontent.com'],
 }
}
